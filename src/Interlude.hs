{-# LANGUAGE FlexibleContexts #-}
module Interlude (module X, module Interlude) where

import Prelude as X hiding (print)
import qualified Prelude
import Data.ByteString as X (ByteString)
import Control.Monad.IO.Class as X
import Control.Monad.Catch as X
import Data.Proxy as X
import Control.Monad.State as X
import Data.Function as X
import Data.Semigroup as X
import Data.String.Conv as X
import Control.Concurrent.MVar as X
import Data.Maybe as X
import Data.Text as X (Text)
import Data.Aeson as X hiding (Error)
import Data.Char as X
import Control.Concurrent as X

pshow :: (StringConv String b, Show a) => a -> b
pshow = toS . show

putText :: MonadIO m => Text -> m ()
putText = liftIO . putStrLn . toS

print :: (Show a, MonadIO m) => a -> m ()
print = liftIO . Prelude.print . show
