module Module1 where

import Interlude

import Control.Effects.Logging

main :: IO ()
main = prettyPrintSummary 100 $
    collectCrumbs $ messagesToCrumbs $ do
        layerLogs (Context "layer1") $ do
            logInfo "Hello from layer1"
            addUserToLogs (LogUser "id" (Just "email@email.com") (Just "username")) $ layerLogs (Context "layer2") $ do
                logInfo "Hello from layer2"
                setDataToShowOf (Just (5 :: Int)) $ logInfo "This message has a data"
            setDataToJsonOf [1, 2, 3 :: Int] $ logInfo "This message has data but is in layer1"
        logWarning "No layers but should still have crumbs"
